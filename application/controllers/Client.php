<?php
/** 
* controlleur client
*/

class Client extends CI_Controller {

/**
* Action : index
* Affichage de liste de clients
*/
    public function index()
	{
	    $data = array();
	    $this->load->model('ModeleClient');
	    $data['list_client1'] = $this->ModeleClient->select_all();
	    $this->load->view('list_client',$data);
	}
/**
* Action : supprimer
* supprimer client 
*/
    public function supprimer()
	{
	    $id_client=$this->input->get_post('id') ;

	    $this->load->model('ModeleClient');
	    $this->ModeleClient->delete($id_client);
	    $this->index();
	}
/**
* Action : Afficher formulaire d'ajout client 
* Formulaire d'ajout client 
*/
    public function ajouter()
	{
	    $this->load->view('form_client');
	}
/**
* Action : Ajouter client 
* Ajouter client 
*/
	public function insert_client()
	{
	    $nom=$this->input->get_post('nom') ;
	    $prenom=$this->input->get_post('prenom') ;
	    $age=$this->input->get_post('age') ;
	    $this->load->model('ModeleClient');
	    $this->ModeleClient->insert($nom,$prenom,$age);
	    $this->index();
	}
/**
* Action : Afficher formulaire modification client 
* Formulaire modification client 
*/
	public function form_modification()
	{
	    $data = array();
	    $id_client=$this->input->get_post('id') ;
	    $this->load->model('ModeleClient');
	    $data['info_client'] = $this->ModeleClient->select_client($id_client);
	    $this->load->view('form_modif_client',$data);
	}
/**
* Action : Modifier informations client

*/
public function update()
	{
	    $nom=$this->input->get_post('nom') ;
	    $prenom=$this->input->get_post('prenom') ;
	    $age=$this->input->get_post('age') ;
	    $id_client=$this->input->get_post('id') ;
	    $this->load->model('ModeleClient');
            $this->ModeleClient->update($nom,$prenom,$age,$id_client);
	    $this->index();
	}
}
